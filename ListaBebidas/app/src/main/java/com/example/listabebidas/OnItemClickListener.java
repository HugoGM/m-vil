package com.example.listabebidas;

public interface OnItemClickListener {
    void onItemClick(Bebida item, int posicion);

}
