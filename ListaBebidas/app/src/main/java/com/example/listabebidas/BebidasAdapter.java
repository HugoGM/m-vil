package com.example.listabebidas;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class BebidasAdapter extends RecyclerView.Adapter<BebidasAdapter.MyViewHolder>{
    private int posicion;
    private final OnItemClickListener listener;
    private List<Bebida>listaB;

    public BebidasAdapter(List<Bebida> listaB,OnItemClickListener listener){
        this.listaB=listaB;
        this.listener=listener;
    }


    @NonNull
    @Override
    public BebidasAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        // toma una vista y generar los objetos y los vincula
        View v=LayoutInflater.from(parent.getContext()).inflate(R.layout.vista_items,parent,false);
        MyViewHolder mVh=new MyViewHolder(v);
        return mVh;
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView nomBebida;
        private TextView ingrediente;
        private EditText precio;
        private CheckBox alcohol;

        public MyViewHolder(@NonNull View itemView ){
            super(itemView);

            nomBebida=(TextView)itemView.findViewById(R.id.tvNombre);
            ingrediente=(TextView)itemView.findViewById(R.id.tvIngrediente);
            precio=(EditText) itemView.findViewById(R.id.etPrecio);
            alcohol=(CheckBox)itemView.findViewById(R.id.cbAlcoholica);
            //precio.setEnabled(false);
            //alcohol.setEnabled(false);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull BebidasAdapter.MyViewHolder holder, final int position){

        //recuperar el valor de la clase del objeto de datos y se lo vamos a asignar a la vista

        final Bebida bebida=listaB.get(position);
        final int pos = position;
        String nB=bebida.getNombre();
        holder.nomBebida.setText(nB);
        String nI=bebida.getIngredienteBase();
        holder.ingrediente.setText(nI);
        float p=bebida.getPrecio();
        holder.precio.setText(String.valueOf(p));
        boolean a=bebida.isAlcoholica();
        holder.alcohol.setChecked(a);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(bebida,pos);
            }
        });

    }

    @Override
    public int getItemCount() {

        return listaB.size();
    }


    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }
}
