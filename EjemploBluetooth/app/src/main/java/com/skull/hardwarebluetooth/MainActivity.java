package com.skull.hardwarebluetooth;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class MainActivity extends AppCompatActivity
{
    Button btn1, btn2, btn3, btn4;
    EditText texto;
    private BluetoothAdapter btAdapter;
    private final int RESP_EN_BT=1, SOL_LIS=2;
    private ConnectThread hiloConexion;
    private ConnectedThread hiloConectado;
    private AcceptThread server;
    private BluetoothSocket socketBT;
    private static final UUID BTUUID=UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private final BroadcastReceiver bReceiver=new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            final String accion=intent.getAction();
            if(BluetoothAdapter.ACTION_STATE_CHANGED.equals(accion));
            {
                final int estado=intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch(estado)
                {
                    case BluetoothAdapter.STATE_OFF:
                            btn1.setText(R.string.TextBH);break;
                    case BluetoothAdapter.STATE_ON:
                            btn1.setText(R.string.TextBD);break;

                    case BluetoothAdapter.STATE_CONNECTING:
                            btn1.setTextColor(Color.BLUE);

                    case BluetoothAdapter.STATE_CONNECTED:
                            Toast.makeText(getApplicationContext(), "Conectado", Toast.LENGTH_LONG).show(); break;
                }
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        texto=(EditText)findViewById(R.id.solicitar);
        btn1=(Button)findViewById(R.id.btnBlue);
        btn2=(Button)findViewById(R.id.btnLst1);
        btn2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intento=new Intent(MainActivity.this, ListaDispositivos.class);
                startActivityForResult(intento, SOL_LIS);
            }
        });
        btn3=(Button)findViewById(R.id.envio);
        btn3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                enviaDatos(texto.getText().toString());
            }
        });

        btn4=(Button)findViewById(R.id.serv);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escuchar();
            }
        });
        verificaBT();
        registrarEventosB();


    }

    private void registrarEventosB()
    {
        IntentFilter filtro=new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);

        this.registerReceiver(bReceiver, filtro);


    }

    private void verificaBT()
    {
        btAdapter=BluetoothAdapter.getDefaultAdapter();
        if(btAdapter==null)
        {
            Toast.makeText(this, "Lo siento no tienes bluetooth", Toast.LENGTH_LONG).show();
            btn1.setEnabled(false);
        }
        else
        {
            if(btAdapter.isEnabled())
            {
                btn1.setText(R.string.TextBD);
            }
            else
            {
                btn1.setText(R.string.TextBH);
            }
        }
    }

    public void accionBoton(View v)
    {
        if(btAdapter.isEnabled())
        {
            btAdapter.disable();
        }
        else
        {
            Intent intentoHB=new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intentoHB, RESP_EN_BT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==RESP_EN_BT)
        {
            if(resultCode==RESULT_OK)
            {

            }
            else
            {
               btn1.setEnabled(false);
            }
        }
        if(requestCode==SOL_LIS)
        {
            if(resultCode==RESULT_OK)
            {
                Toast.makeText(this, data.getStringExtra("ADDRESS"),Toast.LENGTH_LONG).show();
                conectarDBT(data.getStringExtra("ADDRESS"));
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(bReceiver);
    }

    private BluetoothSocket crearBluetoothS(BluetoothDevice bDevice) throws IOException
    {
        return bDevice.createRfcommSocketToServiceRecord(BTUUID);
    }

    private void conectarDBT(String address)
    {
        BluetoothDevice disp=btAdapter.getRemoteDevice(address);
       /* try
        {
            socketBT=crearBluetoothS(disp);
            try
            {
                socketBT.connect();
                hiloConexion=new ConnectThread(disp);
                hiloConexion.run();
            }
            catch (IOException e)
            {
                Toast.makeText(this, "No se ha logrado establecer la conexion", Toast.LENGTH_LONG).show();
            }

        }
        catch (IOException e)
        {
            Toast.makeText(this, "No se ha logrado crear el socket", Toast.LENGTH_LONG).show();
            try
            {
                socketBT.close();
            }
            catch (IOException e1)
            {
                Toast.makeText(this, "No se ha logrado cerrar la conexion el socket", Toast.LENGTH_LONG).show();
            }
        }*/
       hiloConexion=new ConnectThread(disp);
       hiloConexion.start();
    }

    public void enviaDatos(String mensaje)
    {
        hiloConectado.write(mensaje);

    }

    public void escuchar()
    {
        server=new AcceptThread();
        server.start();
    }



    private class ConnectedThread extends  Thread
    {
        private final BluetoothSocket mmSocket;
        private final InputStream inpS;
        private final OutputStream outS;
        private byte[] mmBuffer;

        public ConnectedThread(BluetoothSocket bts)
        {
            mmSocket=bts;
            InputStream tmpIS=null;
            OutputStream tmpOS=null;

            try
            {
                tmpIS=mmSocket.getInputStream();
                tmpOS=mmSocket.getOutputStream();
            }
            catch (IOException e)
            {

            }
            inpS=tmpIS;
            outS=tmpOS;
        }

        @Override
        public void run()
        {
            mmBuffer=new byte[1024];


            int bytes;

            while(true)
            {

                try
                {
                    bytes = inpS.read(mmBuffer);
                    mmBuffer[bytes]=0;

                    String mensaje=new String(mmBuffer);

                    Log.d("BTStream", mensaje);
                }
                catch (IOException ei)
                {

                }

            }
        }

        public void write(String input)
        {
            byte[] msgBuffer=input.getBytes();

            try
            {
                outS.write(msgBuffer);
            }
            catch (IOException e)
            {

            }
        }

        public void canccel()
        {
            try
            {
                mmSocket.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    private class AcceptThread extends Thread {
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            // Use a temporary object that is later assigned to mmServerSocket
            // because mmServerSocket is final.
            BluetoothServerSocket tmp = null;
            try
            {
                // MY_UUID is the app's UUID string, also used by the client code.
                tmp = btAdapter.listenUsingRfcommWithServiceRecord(getResources().getString(R.string.app_name), BTUUID);
            }
            catch (IOException e)
            {
                Log.d("ServerCon:", "Socket's listen() method failed", e);
            }
            mmServerSocket = tmp;
            Log.d("ServerCon:", "Socket's listen() method ok");
        }

        public void run() {
            BluetoothSocket socket = null;
            // Keep listening until exception occurs or a socket is returned.
            while (true) {
                try {
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    Log.d("ServerCon:", "Socket's accept() method failed", e);
                    break;
                }

                if (socket != null) {
                    // A connection was accepted. Perform work associated with
                    // the connection in a separate thread.
                    //manageMyConnectedSocket(socket);
                    hiloConectado=new ConnectedThread(socket);
                    hiloConectado.start();

                    try
                    {
                        mmServerSocket.close();
                    }
                    catch (IOException e)
                    {

                    }
                    break;
                }
            }
        }

        // Closes the connect socket and causes the thread to finish.
        public void cancel() {
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.d("ServerCon:", "Could not close the connect socket", e);
            }
        }
    }

    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            // Use a temporary object that is later assigned to mmSocket
            // because mmSocket is final.
            BluetoothSocket tmp = null;
            mmDevice = device;

            try {
                // Get a BluetoothSocket to connect with the given BluetoothDevice.
                // MY_UUID is the app's UUID string, also used in the server code.
                tmp = device.createRfcommSocketToServiceRecord(BTUUID);
            } catch (IOException e) {
                Log.d("ClienteCon:", "Socket's create() method failed", e);
            }
            mmSocket = tmp;
            Log.d("ClienteCon", "Creado");
        }

        public void run() {
            // Cancel discovery because it otherwise slows down the connection.
            btAdapter.cancelDiscovery();

            try {
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception.
                mmSocket.connect();
                Log.d("ClienteCon:", "Client connexion sucesfully");
            }
            catch (IOException connectException)
            {
                // Unable to connect; close the socket and return.
                try {
                    mmSocket.close();
                } catch (IOException closeException) {
                    Log.d("ClienteCon:", "Could not close the client socket", closeException);
                }
                Log.d("ClienteCon:", "Could not close the client socket");
                return;
            }

            // The connection attempt succeeded. Perform work associated with
            // the connection in a separate thread.
            //manageMyConnectedSocket(mmSocket);
            hiloConectado=new ConnectedThread(mmSocket);
            hiloConectado.start();
        }

        // Closes the client socket and causes the thread to finish.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.d("ClienteCon:", "Could not close the client socket", e);
            }
        }
    }


}
