package com.example.ejemplofragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity {

    private FragmentoPrueba fragmentoPrueba = new FragmentoPrueba();
    //Se encarga de gestionar los fragmentos
    private FragmentManager fragManager = getSupportFragmentManager();
    //Se encarga de crear los fragmentosfag
    private FragmentTransaction fragTrans = fragManager.beginTransaction();
    private Button btnCambiar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCambiar = (Button) findViewById(R.id.btnCambiar);

        fragTrans.add(R.id.contenedorFragmento, fragmentoPrueba);
        fragTrans.commit();

        btnCambiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SegundoFragment segFrag = new SegundoFragment();
                //Se inicia una nueva transaccion porque se queda con la transaccion anterior
                fragTrans = fragManager.beginTransaction();
                //Reemplazamos en el segundo contenedor con el segundo fragmento
                fragTrans.replace(R.id.contenedorFragmento, segFrag);
                //Indicamos que no se puede ir hacia atras con el botón físico del teléfono
                fragTrans.addToBackStack(null);
                fragTrans.commit();
            }
        });
    }
}
