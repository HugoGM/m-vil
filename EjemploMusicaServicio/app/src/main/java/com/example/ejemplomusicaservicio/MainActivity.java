package com.example.ejemplomusicaservicio;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.ejemplomusicaservicio.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private Button btn;
    public int mainValor = 2;
    public static ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        btn = findViewById(R.id.button3);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentoServicio = new Intent(getApplicationContext(), Servicio2.class);
                startService(intentoServicio);
            }
        });


    }

    public void iniciaS(View v){

        Intent intento=new Intent(this,ServicioMusica.class);
        startService(intento);

    }

    public void detener(View v){
        Intent intento=new Intent(this,ServicioMusica.class);
        stopService(intento);
    }


}
