package com.skull.hardwarebluetooth;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Set;

public class ListaDispositivos extends AppCompatActivity
{
    private BluetoothAdapter bluetoothAdapter;
    private ArrayAdapter arrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_dispositivos);

        arrayAdapter= new ArrayAdapter(this, R.layout.vista_item);
        ListView listaDisE=findViewById(R.id.deviceList);
        listaDisE.setAdapter(arrayAdapter);
        listaDisE.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {

                String info=((TextView)view).getText().toString();
                String address=info.substring(info.length()-17);
                Intent intento=new Intent(ListaDispositivos.this, MainActivity.class);
                intento.putExtra("ADDRESS", address);
                setResult(RESULT_OK, intento);
                finish();
            }
        });

        bluetoothAdapter=BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> dispE=bluetoothAdapter.getBondedDevices();

        if(dispE.size()>0)
        {
            for(BluetoothDevice device:dispE)
            {
                arrayAdapter.add(device.getName()+"\n\t"+device.getAddress());
            }
        }
        else
        {
            arrayAdapter.add("No hay dispositivos");
        }

    }
}
