package com.example.mapas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.DateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private FusedLocationProviderClient locationClient;
    private SettingsClient settC;
    private LocationCallback locationCallback;
    private LocationRequest locationRequest;
    private Location currentLocation;
    private String lastUpdateTime;
    private LocationSettingsRequest locationSettingsRequest;
    private TextView latitud, longitud, tiempo;
    private Button mapa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        latitud = (TextView) findViewById(R.id.latitud);
        longitud = (TextView) findViewById(R.id.longitud);
        tiempo = (TextView) findViewById(R.id.tiempo);
        mapa = (Button) findViewById(R.id.mapa);
        locationClient = LocationServices.getFusedLocationProviderClient(this);
        settC = LocationServices.getSettingsClient(this);
        createLocationCallback();
        crearLocationRequest();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        locationSettingsRequest = builder.build();

        mapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intento = new Intent(getApplicationContext(), MapsActivity.class);
                intento.putExtra("latitud", currentLocation.getLatitude());
                intento.putExtra("longitud", currentLocation.getLongitude());
                startActivity(intento);
            }
        });
    }

    public void createLocationCallback() {
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                currentLocation = locationResult.getLastLocation();
                lastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                actualizaUI();
            }
        };

    }

    private void actualizaUI() {
        //Poner referencia a los textview de la latitud longitud  y tiempo
        longitud.setText(String.valueOf(currentLocation.getLongitude()));
        latitud.setText(String.valueOf(currentLocation.getLatitude()));
        tiempo.setText(lastUpdateTime);
    }

    public void crearLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    protected void onResume() {
        super.onResume();
        int permState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if(permState == PackageManager.PERMISSION_GRANTED) {
            iniciaActulizacion();
        }
    }

    public void iniciaActulizacion() {
        settC.checkLocationSettings(locationSettingsRequest).
                addOnSuccessListener(this,
                        new OnSuccessListener<LocationSettingsResponse>() {
                            @Override
                            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                                Log.d("App: ", "Se cumplen las caracteristica");
                                locationClient.requestLocationUpdates(
                                        locationRequest, locationCallback, Looper.myLooper()
                                );
                            }
                        }).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("App: ", "No se cumplen las caracteristica");
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(locationClient != null) {
            locationClient.removeLocationUpdates(locationCallback);
        }
    }
}
