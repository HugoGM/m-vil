package com.example.listabebidas;

import android.provider.BaseColumns;

public class EsquemaBebidas {

    public static abstract class BebidasEnty implements BaseColumns{
        public static final String TABLA_BEBIDAS="bebidas";
        public static final String ID="id";
        public static final String NOMBRE="nombre";
        public static final String INGREDIENTE="ing";
        public static final String PRECIO="precio";
        public static final String ALCOHOLICA="alcoholica";
    }
}
