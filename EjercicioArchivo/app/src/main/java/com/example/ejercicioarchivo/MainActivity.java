package com.example.ejercicioarchivo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class MainActivity extends AppCompatActivity {

    private Button btnLeer, btnPref;
    private TextView txtMostrar;
    private AssetManager astm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLeer = (Button) findViewById(R.id.btnLeer);
        btnPref = (Button) findViewById(R.id.btnPref);
        txtMostrar = (TextView) findViewById(R.id.txtMostrar);

        btnLeer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Leyendo archivos dede rar y assets
                //InputStream f1 = getResources().openRawResource(R.raw.uno);
                /*astm = getApplicationContext().getAssets();
                InputStream f1 = null;
                try {
                    f1 = astm.open("dos.txt");
                    InputStreamReader r = new InputStreamReader(f1);
                    BufferedReader b = new BufferedReader(r);
                    txtMostrar.setText(b.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }*/

                //Creando archivos en el almacenamiento interno
                /*
                FileOutputStream ofo = null;
                try {
                    ofo = openFileOutput("tres.txt", Context.MODE_PRIVATE);
                    OutputStreamWriter fout = new OutputStreamWriter(ofo);
                    BufferedWriter b = new BufferedWriter(fout);
                    PrintWriter pw = new PrintWriter(fout);
                    pw.println("dsfsdf");
                    pw.close();
                    ofo.close();
                    fout.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }*/



            }
        });

        btnPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sp = getSharedPreferences("prueba", Context.MODE_PRIVATE);
                String n = sp.getString("Nombre", " sin nombre");
                txtMostrar.setText("Hola " + n);
            }
        });

        //Escribiendo en archivos de preferencias
        SharedPreferences sp = getSharedPreferences("prueba", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("Nombre", "ksjdf");
        editor.commit();
    }



}
