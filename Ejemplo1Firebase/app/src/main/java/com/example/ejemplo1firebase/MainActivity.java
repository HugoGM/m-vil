package com.example.ejemplo1firebase;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    FirebaseDatabase database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        asignarEscucha();
      /*  // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("message");

        myRef.setValue("Hello, World!");*/
    }

    private void enviaDatos(View v){

        // Write a message to the database

        DatabaseReference myRef = database.getReference("registros");

        //myRef.setValue("valor1");
        Random rand=new Random();
       // String val="datos"+rand.nextInt();

        myRef.child("registro").push().setValue(new Registro("cadena",56,true));

    }


    public void asignarEscucha(){
        DatabaseReference myRef = database.getReference("datos");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                TextView eti=(TextView) findViewById(R.id.textView);
                eti.setText(value);
                Log.d("EX", "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("EX", "Failed to read value.", error.toException());
            }
        });


    }

}
