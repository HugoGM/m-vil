package com.example.ejemplo1firebase;

public class Registro {

    private String campo1;
    private int campo2;
    private boolean campo3;

    public Registro(String campo1, int campo2, boolean campo3) {
        this.campo1 = campo1;
        this.campo2 = campo2;
        this.campo3 = campo3;
    }

    public String getCampo1() {
        return campo1;
    }

    public void setCampo1(String campo1) {
        this.campo1 = campo1;
    }

    public int getCampo2() {
        return campo2;
    }

    public void setCampo2(int campo2) {
        this.campo2 = campo2;
    }

    public boolean isCampo3() {
        return campo3;
    }

    public void setCampo3(boolean campo3) {
        this.campo3 = campo3;
    }
}
