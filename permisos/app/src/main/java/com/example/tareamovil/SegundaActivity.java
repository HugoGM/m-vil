package com.example.tareamovil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SegundaActivity extends AppCompatActivity {

    String mensaje;
    TextView etiqueta;
    Button boton;
    EditText texEdi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);


        mensaje= getIntent().getStringExtra("VALOR");
        etiqueta =findViewById(R.id.textView2);
        etiqueta.setText(mensaje);
        boton = (Button) findViewById(R.id.button2);
        texEdi = (EditText) findViewById(R.id.editText);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentoR = new Intent();

                intentoR = new Intent();
                intentoR.putExtra("resp", texEdi.getText().toString());
                setResult(RESULT_OK,intentoR);
                finish();


            }
        });





    }


}
