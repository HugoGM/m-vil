package com.example.listabebidas;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.widget.Toast;

import java.util.ArrayList;

public class ManejadorBD {
 private   Context ctx;
private static BebidasDbHelper helper;

    public ManejadorBD(Context c){
        ctx=c;
        helper=new BebidasDbHelper(c);
        //helper.cargaInicial();
    }

    public void muestra(ArrayList<Bebida>lB){

        Cursor res=helper.consulta();

        while (res.moveToNext()){
            String nombre=res.getString(res.getColumnIndex(EsquemaBebidas.BebidasEnty.NOMBRE));
            //Toast.makeText(ctx,nombre,Toast.LENGTH_LONG).show();
            String ingrediente=res.getString(res.getColumnIndex(EsquemaBebidas.BebidasEnty.INGREDIENTE));

            float precio=res.getFloat(res.getColumnIndex(EsquemaBebidas.BebidasEnty.PRECIO));

            boolean alcoholica=res.getInt(res.getColumnIndex(EsquemaBebidas.BebidasEnty.ALCOHOLICA))==1?true:false;

            Bebida nBeb = new Bebida(nombre,ingrediente,alcoholica,precio);
            nBeb.set_id(res.getLong((res.getColumnIndex(EsquemaBebidas.BebidasEnty._ID))));

            lB.add(nBeb);
        }
    }

    public boolean agregar(Bebida b){

         ContentValues valores=new ContentValues();
        valores.put(EsquemaBebidas.BebidasEnty.NOMBRE,b.getNombre());
        valores.put(EsquemaBebidas.BebidasEnty.INGREDIENTE,b.getIngredienteBase());
        valores.put(EsquemaBebidas.BebidasEnty.PRECIO,b.getPrecio());
        String val=b.isAlcoholica()?"1":"0";
        valores.put(EsquemaBebidas.BebidasEnty.ALCOHOLICA,val);

        Long id = helper.insertar(valores);
        if(id == -1) return false;

        b.set_id(id);
        return true;

    }

    public boolean borrarItem(long id) {

        return helper.delete(id);
    }



}
