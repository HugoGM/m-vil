package com.example.ejemplocamaragaleria;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private Button btnCargaGaleria;
    private ImageView imgViewImagen;
    private static final int SELECT_IMAG = 10;
    private static final int TOMAR_FOTO = 9;
    private final String CARPETA_RAIZ = "imagenesPrueba";
    private final String RUTA_IMAGEN = CARPETA_RAIZ + "/foto";
    private String path;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCargaGaleria = (Button) findViewById(R.id.btnCargaGaleria);
        imgViewImagen = (ImageView) findViewById(R.id.imgViewImagen);

        btnCargaGaleria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //seleccionaImagen();
                tomarFoto();
            }
        });

    }


    private void seleccionaImagen() {
        Intent intento = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intento.setType("image/");
        startActivityForResult(intento.createChooser(intento, "Selecciona la aplicación "), SELECT_IMAG);
    }


    private void tomarFoto() {
       /* File fileImagen = Environment.getExternalStorageDirectory(), RUTA_IMAHEN;
        String nomImagen = String.valueOf(System.currentTimeMillis() / 100) + "jpg";
        path = Environment.getExternalStorageDirectory()
                + File.separator + RUTA_IMAGEN + File.separator + nomImagen;
        File imagen = new File(path);
        Intent intento = new Intent((MediaStore.ACTION_IMAGE_CAPTURE));
        intento.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imagen));
        startActivityForResult(intento, TOMAR_FOTO);*/

        Intent intento = new Intent((MediaStore.ACTION_IMAGE_CAPTURE));
        startActivityForResult(intento, TOMAR_FOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == SELECT_IMAG && resultCode == RESULT_OK) {
            Uri path = data.getData();
            imgViewImagen.setImageURI(path);
            Toast.makeText(getApplicationContext(), path.toString(), Toast.LENGTH_LONG).show();

        }

        Toast.makeText(getApplicationContext(), String.valueOf(resultCode), Toast.LENGTH_LONG).show();

        if(requestCode == TOMAR_FOTO && resultCode == RESULT_OK) {
            /*MediaScannerConnection.scanFile(this, new String[] {path}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String s, Uri uri) {
                            Log.d("X: ", "path = " + path);
                        }
                    });
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            imgViewImagen.setImageBitmap(bitmap);*/
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            imgViewImagen.setImageBitmap(bitmap);
        }
    }
}
