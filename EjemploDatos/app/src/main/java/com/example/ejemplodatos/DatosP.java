package com.example.ejemplodatos;

import android.os.Parcel;
import android.os.Parcelable;

public class DatosP implements Parcelable {

    private String nombre;
    private int edad;
    private float estatura;

    public DatosP(String nombre, int edad, float estatura) {
        this.nombre = nombre;
        this.edad = edad;
        this.estatura = estatura;
    }

    public DatosP() {

    }

    protected DatosP(Parcel in) {
        nombre = in.readString();
        edad = in.readInt();
        estatura = in.readFloat();
    }


    public static final Creator<DatosP> CREATOR = new Creator<DatosP>() {
        @Override
        public DatosP createFromParcel(Parcel in) {
            return new DatosP(in);
        }

        @Override
        public DatosP[] newArray(int size) {
            return new DatosP[size];
        }
    };

    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }

    public float getEstatura() {
        return estatura;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setEstatura(float estatura) {
        this.estatura = estatura;
    }

    @Override
    public String toString() {
        return "Datos{" +
                "nombre='" + nombre + '\'' +
                ", edad=" + edad +
                ", estatura=" + estatura +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    parcel.writeString(nombre);
    parcel.writeInt(edad);
    parcel.writeFloat(estatura);
    }
}
