package com.example.ejemploservicios;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class ServiceEjem extends Service {
    public ServiceEjem() {
    }

    /*Permite establecer un canal de comunicacón con el servicio*/
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    //Se crea el servicio
    @Override
    public void onCreate() {
        super.onCreate();

        Log.d("S:", "Servicio creado");
    }

    //Lanza el servicio
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("S:", "Servicio Iniciado");
        ciclo();
        return Service.START_REDELIVER_INTENT; //Service.START_STICKY //super.onStartCommand(intent, flags, startId);
    }

    //Detiene el servicio
    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d("S:", "Servicio destruido");
    }

    private void ciclo() {
        int i = 0;
        while(i < 100000) {
            Log.d("S:", String.valueOf(i));
            i++;
        }
    }
}
