package com.example.ejemploservicios;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnServicioIniciar;
    private Button btnServicioDetener;
    private Button btnServicio;
    Intent intentoService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnServicioIniciar = (Button) findViewById(R.id.btnServicioIniciar);
        btnServicioDetener = (Button) findViewById(R.id.btnServicioDetener);
        btnServicio = (Button) findViewById(R.id.btnServicio);
        intentoService = new Intent(getApplicationContext(), ServiceEjem.class);

        //
        btnServicioIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startService(intentoService);
            }
        });

        //Método para detener el servicio
        btnServicioDetener.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intento2 = new Intent(getApplicationContext(), ServiceEjem.class);
                stopService(intentoService);
            }
        });

        //Los servicios no se lazan simultaneamente, se encolan
        // y hasta que se termino un servicio inicio otro
        btnServicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("S2: ", "Se lanzo de nuevo");

                Intent in = new Intent(getApplicationContext(), ServiceEjem.class);
                startService(in);
            }
        });
    }
}
