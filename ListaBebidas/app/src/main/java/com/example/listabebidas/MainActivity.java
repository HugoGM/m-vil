package com.example.listabebidas;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView listaRV;
    private ArrayList<Bebida> bebidas;
    private BebidasAdapter adapter;
    private ManejadorBD manejadorBD;
    private long item_select;
    private int posAct;
    private static final int CODIGO_RESULTADO = 1;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflaterM=getMenuInflater();
        inflaterM.inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        listaRV=(RecyclerView)findViewById(R.id.listaBebidas);
        //inicializaDatos();

        //Cargamos los datos de la base de datos en el Recycler
        bebidas=new ArrayList<Bebida>();
        if(manejadorBD == null) {
            manejadorBD=new ManejadorBD(this);
            manejadorBD.muestra(bebidas);
        }

        //Gestos
        ItemTouchHelper.SimpleCallback gestos = new ItemTouchHelper.SimpleCallback
                (0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView,
                                  @NonNull RecyclerView.ViewHolder viewHolder,
                                  @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                Toast.makeText(getApplicationContext(), viewHolder.getAdapterPosition() + ",", Toast.LENGTH_LONG).show();
            }
        };

        //Agregando el gesto al recycler
        ItemTouchHelper itm = new ItemTouchHelper(gestos);
        itm.attachToRecyclerView(listaRV);

        //Recuperamos el item seleccionado
        adapter=new  BebidasAdapter(bebidas, new OnItemClickListener() {
            @Override
            public void onItemClick(Bebida item, int pos) {
                //Toast.makeText(getApplicationContext(),item.toString(),Toast.LENGTH_SHORT).show();
                item_select = item.get_id();
                Toast.makeText(getApplicationContext(),String.valueOf(pos),Toast.LENGTH_SHORT).show();
                posAct = pos;
            }
        });

        listaRV.setAdapter(adapter);
        listaRV.setHasFixedSize(true);
        listaRV.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.agregar:
                Intent intento = new Intent(getApplicationContext(), FormularioAgregar.class);
                //startActivity(intento);
                startActivityForResult(intento, CODIGO_RESULTADO);
               /*Bebida b= new Bebida("mojito","ron_menta",true,40.4f);
               if(manejadorBD.agregar(b)){

                   bebidas.add(b);

                   //adapter.notifyItemInserted(bebidas.size() - 1);
                   //adapter.notifyItemInserted(bebidas.indexOf(b));
                   //adapter.notifyDataSetChanged();
                   adapter.notifyItemRangeInserted(bebidas.size() - 1, 1);

               }*/
              // adapter.notifyDataSetChanged();

                break;
            case R.id.borrar:

                if(posAct != -1 && posAct < bebidas.size() && manejadorBD.borrarItem(item_select)) {
                    bebidas.remove(posAct);
                    adapter.notifyItemRemoved(posAct);
                    adapter.notifyItemRangeRemoved(posAct, bebidas.size());

                }


        }

        return super.onOptionsItemSelected(item);
    }

    private void inicializaDatos(){

        /*bebidas=new ArrayList<Bebida>();
        bebidas.add(new Bebida("limonada","limon",false,20.6f));
        bebidas.add(new Bebida("mojito","ron",true,38.4f));
        bebidas.add(new Bebida("cerveza","cebada",true,30.5f));*/

        //bd.muestra();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CODIGO_RESULTADO && resultCode == RESULT_OK) {
            Bebida bebida = data.getExtras().getParcelable("bebida");

            if(manejadorBD.agregar(bebida)){

                bebidas.add(bebida);

                //adapter.notifyItemInserted(bebidas.size() - 1);
                //adapter.notifyItemInserted(bebidas.indexOf(b));
                //adapter.notifyDataSetChanged();
                adapter.notifyItemRangeInserted(bebidas.size() - 1, 1);

            }
        }


    }


}
