package com.example.intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button boton, btnFoto;
    private static final int CODIGO = 1;
    private static final int CODIGO_PERMISO_MARCADO = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boton = (Button) findViewById(R.id.button);
        btnFoto = (Button) findViewById(R.id.button2);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intento = new Intent(getApplicationContext(), SegundaActivity.class);
                intento.putExtra("valor", "asd");
 |               startActivityForResult(intento, CODIGO);*/

                Intent intentoImplicito = new Intent(Intent.ACTION_CALL, Uri.parse("tel: 5512348569"));
                //intentoImplicito.setData(Uri.parse("tel: 5512348569"));

                int permisoMarcar = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);

                if (permisoMarcar != getPackageManager().PERMISSION_GRANTED) {

                } else {
                    startActivity(intentoImplicito);
                }
            }

        });

        btnFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Intent intentoImplicito = new Intent(Intent.ACTION_CAMERA_BUTTON);
                //startActivity(intentoImplicito);

            }
        });

    }

    public void pidePermiso() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE) &&
            ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {


        } else {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, CODIGO_PERMISO_MARCADO);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, CODIGO_PERMISO_MARCADO);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == CODIGO_PERMISO_MARCADO) {
            //if(grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {

            Toast.makeText(this, "Permiso concedido", Toast.LENGTH_LONG).show();
            //}
        }
    }
}
