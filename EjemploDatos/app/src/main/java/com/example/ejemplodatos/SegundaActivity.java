package com.example.ejemplodatos;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class SegundaActivity extends AppCompatActivity {

    private TextView txtNombre;
    private TextView txtEdad;
    private TextView txtEstatura;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);

        //Objeto serializable
        /*
        Datos datos = (Datos) getIntent().getSerializableExtra("sDatos");

        txtNombre = (TextView) findViewById(R.id.txtNombre);
        txtEdad = (TextView) findViewById((R.id.txtEdad));
        txtEstatura = (TextView) findViewById(R.id.txtEstatura);

        txtNombre.setText(datos.getNombre());
        txtEdad.setText(String.valueOf(datos.getEdad()));
        txtEstatura.setText(String.valueOf(datos.getEstatura()));
         */


        //Objeto Parcelable

        DatosP datos = getIntent().getParcelableExtra("pDatos");

        txtNombre = (TextView) findViewById(R.id.txtNombre);
        txtEdad = (TextView) findViewById((R.id.txtEdad));
        txtEstatura = (TextView) findViewById(R.id.txtEstatura);

        txtNombre.setText(datos.getNombre());
        txtEdad.setText(String.valueOf(datos.getEdad()));
        txtEstatura.setText(String.valueOf(datos.getEstatura()));
    }
}
