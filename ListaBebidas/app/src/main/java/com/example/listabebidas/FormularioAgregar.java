package com.example.listabebidas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class FormularioAgregar extends AppCompatActivity {
    private EditText txtNombre, txtIngrediente, txtPrecio;
    private CheckBox chkAlcoholica;
    private Button btnGuardar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_agregar);

        DisplayMetrics medidasV = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(medidasV);
        //Obtenemos el ancho de la ventana
        int ancho = medidasV.widthPixels;
        //Obtemos el alto de la ventana
        int alto = medidasV.heightPixels;
        //Reducimos el ancho y alto de la ventana, lo establecemos en 80% y 50% resptectivamente
        getWindow().setLayout((int) (ancho * 0.8), (int) (alto * 0.5));

        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtIngrediente = (EditText) findViewById(R.id.txtIngrediente);
        txtPrecio = (EditText) findViewById(R.id.txtPrecio);
        chkAlcoholica = (CheckBox) findViewById(R.id.chkAlcoholica);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bebida beb = new Bebida(txtNombre.getText().toString(), txtIngrediente.getText().toString(), chkAlcoholica.isChecked(), Float.valueOf(txtPrecio.getText().toString()));
                Intent intentResultado = new Intent();
                intentResultado.putExtra("bebida", beb);
                setResult(RESULT_OK, intentResultado);
                finish();
            }
        });




    }
}
