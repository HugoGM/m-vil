package com.example.listabebidas;

import android.os.Parcel;
import android.os.Parcelable;

public class Bebida implements Parcelable {
    // clase POJO se usa para modelar datos

    private String nombre;
    private String ingredienteBase;
    private boolean alcoholica;
    private float precio;
    private Long _id;

    public Bebida(String nombre, String ingredienteBase, boolean alcoholica, float precio) {
        this.nombre = nombre;
        this.ingredienteBase = ingredienteBase;
        this.alcoholica = alcoholica;
        this.precio = precio;
    }

    protected Bebida(Parcel in) {
        nombre = in.readString();
        ingredienteBase = in.readString();
        alcoholica = in.readByte() != 0;
        precio = in.readFloat();
    }

    public static final Creator<Bebida> CREATOR = new Creator<Bebida>() {
        @Override
        public Bebida createFromParcel(Parcel in) {
            return new Bebida(in);
        }

        @Override
        public Bebida[] newArray(int size) {
            return new Bebida[size];
        }
    };

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIngredienteBase() {
        return ingredienteBase;
    }

    public void setIngredienteBase(String ingredienteBase) {
        this.ingredienteBase = ingredienteBase;
    }

    public boolean isAlcoholica() {
        return alcoholica;
    }

    public void setAlcoholica(boolean alcoholica) {
        this.alcoholica = alcoholica;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    @Override
    public String toString(){
        return "Bebida{" +
                "nombre='"+nombre+'\''+
                ",ingredienteBase='" + ingredienteBase + '\'' +
                ",alcoholica="+ alcoholica +
                ",precioP=" + precio +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(nombre);
        parcel.writeString(ingredienteBase);
        parcel.writeByte((byte) (alcoholica ? 1 : 0));
        parcel.writeFloat(precio);
    }
}
