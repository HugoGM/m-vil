package com.example.ejemplodatos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText editNombre;
    private EditText editEdad;
    private EditText editEstatura;
    private Button btnEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editNombre = (EditText) findViewById(R.id.editNombre);
        editEdad = (EditText) findViewById(R.id.editEdad);
        editEstatura = (EditText) findViewById(R.id.editEstatura);
        btnEnviar = (Button) findViewById(R.id.btnEnviar);

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Objeto serializable
                /*
                Intent intentoDato = new Intent(getApplicationContext(), SegundaActivity.class);

                Datos datos = new Datos(editNombre.getText().toString(),
                        Integer.valueOf(editEdad.getText().toString()),
                        Float.valueOf(editEstatura.getText().toString()));

                 intentoDato.putExtra("sDatos", datos);
                 startActivity(intentoDato);*/

                //Objeto parcellable

                Intent intento = new Intent(getApplicationContext(), SegundaActivity.class);

                DatosP datosP = new DatosP(editNombre.getText().toString(),
                        Integer.valueOf(editEdad.getText().toString()),
                        Float.valueOf(editEstatura.getText().toString()));

                intento.putExtra("pDatos", datosP);

                startActivity(intento);




            }
        });
    }
}
