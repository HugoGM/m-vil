package com.example.listabebidas;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BebidasDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION=1;
    public static final String DATABASE_NAME="Bebidas.db";
    SQLiteDatabase bd;

    public BebidasDbHelper(Context c){

        super(c,DATABASE_NAME,null,DATABASE_VERSION);
        bd=getWritableDatabase(); // objetos que devuelven instancias de nuestra base de datos
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //aqui ingresamos las sentencias par la creacion de BD
        sqLiteDatabase.execSQL(" CREATE TABLE "+EsquemaBebidas.BebidasEnty.TABLA_BEBIDAS +
                "("+
                EsquemaBebidas.BebidasEnty._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"+
                EsquemaBebidas.BebidasEnty.NOMBRE + " TEXT NOT NULL," +
                EsquemaBebidas.BebidasEnty.INGREDIENTE + " TEXT NOT NULL," +
                EsquemaBebidas.BebidasEnty.PRECIO + " NUMBER," +
                EsquemaBebidas.BebidasEnty.ALCOHOLICA + " INTEGER)"
                );

        cargaInicial(sqLiteDatabase);

        /*ContentValues valores=new ContentValues();
        valores.put(EsquemaBebidas.BebidasEnty.NOMBRE,"naranjada");
        valores.put(EsquemaBebidas.BebidasEnty.INGREDIENTE,"naranja");
        valores.put(EsquemaBebidas.BebidasEnty.PRECIO,"20.5");
        valores.put(EsquemaBebidas.BebidasEnty.ALCOHOLICA,"0");

        sqLiteDatabase.insert(EsquemaBebidas.BebidasEnty.TABLA_BEBIDAS,null,valores);*/


    }

    public boolean delete(long id){
        String args[] = {Long.toString(id)};
        int i = bd.delete(EsquemaBebidas.BebidasEnty.TABLA_BEBIDAS, "_ID=?", args);

        if(i == 0) return false;

        return true;

    }

    public long insertar(ContentValues values){

       return bd.insert(EsquemaBebidas.BebidasEnty.TABLA_BEBIDAS,null,values);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public Cursor consulta(){
        String columnas[]=new String[]{"*"};

        return bd.query(EsquemaBebidas.BebidasEnty.TABLA_BEBIDAS,columnas,null,null,null,null,null);
    }

    public void cargaInicial(SQLiteDatabase bd){
        ContentValues valores = new ContentValues();
        valores.put(EsquemaBebidas.BebidasEnty.NOMBRE,"naranjada");
        valores.put(EsquemaBebidas.BebidasEnty.INGREDIENTE,"naranja");
        valores.put(EsquemaBebidas.BebidasEnty.PRECIO,"20.5");
        valores.put(EsquemaBebidas.BebidasEnty.ALCOHOLICA,"0");

        bd.insert(EsquemaBebidas.BebidasEnty.TABLA_BEBIDAS,null,valores);

        valores.put(EsquemaBebidas.BebidasEnty.NOMBRE,"fresada");
        valores.put(EsquemaBebidas.BebidasEnty.INGREDIENTE,"fresa");
        valores.put(EsquemaBebidas.BebidasEnty.PRECIO,"20.5");
        valores.put(EsquemaBebidas.BebidasEnty.ALCOHOLICA,"0");

        bd.insert(EsquemaBebidas.BebidasEnty.TABLA_BEBIDAS,null,valores);


        valores.put(EsquemaBebidas.BebidasEnty.NOMBRE,"limonada");
        valores.put(EsquemaBebidas.BebidasEnty.INGREDIENTE,"limon");
        valores.put(EsquemaBebidas.BebidasEnty.PRECIO,"20.5");
        valores.put(EsquemaBebidas.BebidasEnty.ALCOHOLICA,"0");

        bd.insert(EsquemaBebidas.BebidasEnty.TABLA_BEBIDAS,null,valores);


        valores.put(EsquemaBebidas.BebidasEnty.NOMBRE,"cerveza");
        valores.put(EsquemaBebidas.BebidasEnty.INGREDIENTE,"cebada");
        valores.put(EsquemaBebidas.BebidasEnty.PRECIO,"35.5");
        valores.put(EsquemaBebidas.BebidasEnty.ALCOHOLICA,"1");

        bd.insert(EsquemaBebidas.BebidasEnty.TABLA_BEBIDAS,null,valores);
        //bd=getWritableDatabase();

    }
}
