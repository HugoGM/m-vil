package com.example.tareamovil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    private final static int  codigo=1;
    private final static int CODIGO_PERMISO_MARCADO=1;
    private Button boton;
    int perminoMarcar;

    Button botonM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        perminoMarcar = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);
        boton = (Button) findViewById(R.id.button);
        botonM = (Button) findViewById(R.id.btnMarcar);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intento = new Intent(getApplicationContext(), SegundaActivity.class);
                intento.putExtra("VALOR","Hola");
                startActivityForResult(intento,codigo);


            }
        });

        botonM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //devuelve algo entero

                Intent intentoImp = new Intent(Intent.ACTION_CALL);

               if (perminoMarcar != getPackageManager().PERMISSION_GRANTED) {
                   pidePermiso();

               }else {
                   marcar();
               }


            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == codigo){
            String respuesta = data.getStringExtra("resp");
            Toast.makeText(this, respuesta, Toast.LENGTH_LONG).show();
        }
    }

    private  void pidePermiso(){

        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)) {


        }else{

            ActivityCompat.requestPermissions(this , new String[]{Manifest.permission.CALL_PHONE}, CODIGO_PERMISO_MARCADO);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == CODIGO_PERMISO_MARCADO){
            int perminoMarcar = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);
            if(grantResults.length>0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED)){
              marcar();

            }
        }
    }

    private void marcar(){
        int perminoMarcar = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);
        Intent intentoImp = new Intent(Intent.ACTION_CALL);
        intentoImp.setData(Uri.parse("tel: 551587"));
        if (perminoMarcar != getPackageManager().PERMISSION_GRANTED) {
            pidePermiso();

        }else {
            startActivity(intentoImp);
        }
    }
}
