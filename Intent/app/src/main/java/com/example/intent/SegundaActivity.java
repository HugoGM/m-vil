package com.example.intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SegundaActivity extends AppCompatActivity {

    private String mensaje;
    private TextView texto;
    private Button boton;
    private EditText editarNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);

        texto = (TextView) findViewById(R.id.textView);
        boton = (Button) findViewById(R.id.button2);
        editarNombre = (EditText) findViewById(R.id.editText);
        mensaje = getIntent().getStringExtra(("valor"));
        texto.setText(mensaje);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentoR = new Intent();
                intentoR.putExtra("resp", editarNombre.getText().toString());
                setResult(RESULT_OK, intentoR);
                finish();
            }
        });

    }
}
